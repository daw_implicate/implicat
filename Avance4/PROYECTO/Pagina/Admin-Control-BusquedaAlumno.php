<!doctype html>
<html lang="en">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans" rel="stylesheet"> 
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="maincolors.css">

    <title>Admin-Busqueda-Por-Alumno</title>
  </head>
  <body>
<!-- SESION ACTIVA PHP -->

<?php require("sesion_activa.php"); ?>







<!-- HEADER -->
   <header >
    <div class="container-fluid mitad1" >
      <img src="Images/img-nav2.png" class="img-responsive barra">
      <?php
     echo "<h4 class='saludo'> Usuario : ". $_SESSION["usuario"]."</h4>";
      ?> 




    </div>

</header>


<!--
      Documento 


-->
<div class="header_bg img-responsive">
  <div class="container">
    
    <div class="row header ">
      
    <div class="logo navbar-left">
      <h1>
        BUSQUEDA POR ALUMNO
      </h1>
    </div>
    <div class="clearfix"></div>
    
  </div>
 
</div>
<nav class="navbar navbar-default  menu container">
  <div class="container navi">
    <div class="navbar-header">
      
    </div>
    <ul class="nav navbar-nav ">
      <li ><a href="Admin.php">EQUIPOS</a></li>
      <li ><a href="Admin-Usuarios.php">USUARIOS</a></li>
      <li class="activa"><a href="Admin-Control.php">ARCHIVOS</a></li>
      <li><a href="Admin-Eventos.php">CONTROL</a></li>
    </ul>
    <a href="cierre_sesion.php"><img src="Images/logout.png" class="img-responsive salir"></a>
  </div>
</nav>

</div>
<div class="clearfix"></div>


<section class="main row">

    <article class="container">

      <!--Busqueda-->
     <div class="row header container ">
      <div class="navbar-left">
              <h2 class="subtitulo"> <b>Nombre : </b> Andres Padres </h2>
      <h2 class="subtitulo"> <b>ID :  </b> 0015</h2>
      <h2 class="subtitulo"> <b>Equipo: </b>8</h2>
      <h2 class="subtitulo"> <b>Rol: </b>Seguridad</h2>
      </div>
        <div class="h_search navbar-right">
          <form>
            <input class="text" value="Busqueda por alumno" onfocus="this.value ='';" onblur="if(this.value==''){this.value='Busqueda por alumno';}" type="text"></input>
              <a href="Admin-Control-BusquedaAlumno.php"><input type="button" value="buscar" name="busqueda"></a>
          </form>
        </div>
      </div>
<!--Documento-->



<div class="form-group busqueda">
  <div class="form-inline">
    <label class="sr-only">Seleccionar Fecha</label>
    <select class="form-control" id="fechaA" name="fecha">
      <option> Seleccionar Fecha</option>
      <option>02/02/18</option>
      <option>08/02/18</option>
    </select>
  </div>
</div>

    
<!-- TABLA DE TRABAJOS  -->

<table>
  <caption>Trabajos</caption>
  <thead>
    <tr>
      <th scope="col">Fecha</th>
      <th scope="col">Trabajo</th>
      
    </tr>
  </thead>
  <tbody>
    <tr>
      <td data-label="Fecha">02/02/18</td>
      <td data-label="Trabajo"><a href="#">Plantilla Seguridad</a></td>
    </tr>
    <tr>
      <td data-label="Fecha">08/02/18</td>
      <td data-label="Trabajo"><a href="#">Plantilla Seguridad</a></td>
    </tr>
    <tr>
      <td data-label="Fecha">16/02/18</td>
      <td data-label="Trabajo"><a href="#">Plantilla Seguridad</a></td>
    </tr>
    <tr>
      <td data-label="Fecha">21/02/18</td>
      <td data-label="Trabajo"><a href="#">Plantilla Seguridad</a></td>
    </tr>
  </tbody>
</table>

</div></article>




</section>

<div class="row col-md-2">
  <a href="Admin-Control.php"><img src="Images/atras.png" class=" atras img-responsive"></a>
</div>


<footer >
 <div class=" container-fluid borde">
   
 </div>
</footer>














    <!-- Scripts -->



      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
   <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>-->
  </body>
</html>