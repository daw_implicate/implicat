<!doctype html>
<html lang="en">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans" rel="stylesheet"> 
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="mainUser.css">
    <link rel="stylesheet" type="text/css" href="maincolors.css">

    <title>Usuario-Inicio</title>
  </head>
  <body>

    <!-- SESSION -->
<?php require("sesion_activa.php") ;
include("Partials/Header.php") ;?>







<!-- HEADER -->



<!--
      Documento 


-->



<!-- USUARIOS AUTORIZADOS -->
<?php


include("Partials/Navegacion.php");


?>

<!-- FIN DE BARRA DE NAVEGACION -->

<!-- CONTENIDO DEL USUARIO  -->
<!-- VISUALIZAR EQUIPO  -->
<?php if($_SESSION['rol'] == 3) { include("Partials/GeneralesEquipo.php"); }?>

<br><br><br>



<!-- VISUALIZAR CARRETE DE FOTOS  -->

<?php if($_SESSION['rol'] == 3) { ?>
<div class="container-fluid fondo">
<div class="container carrete"> 
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="Images/la.png" alt="Los Angeles" width="100%">
      </div>

      <div class="item">
        <img src="Images/chicago.png" alt="Chicago" width="100%">
      </div>
    
      <div class="item">
        <img src="Images/new.png" alt="New york" width="100%">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Anterior</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Siguiente</span>
    </a>
  </div>
</div>

</div>

<?php } ?>

<!--  FIN DE VISUALIZAR CARRETE DE FOTOS  -->
<?php if($_SESSION['rol'] == 3) { ?>
<div class="container">
<section class="main row">
  <article class="container">

<br><br><br>
<p></p>
<p></p>


<!-- INTEGRANTES -->
<table>
  <caption>Integrantes de Equipo</caption>
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Nombre</th>
      <th scope="col">Apellido</th>
      <th scope="col">Rol</th>
  </thead>
  <tbody>
    <tr>
      <td data-label="IdU">0001</td>
      <td data-label="Nombre">Andres</td>
      <td data-label="Apellido">Gonzalez</td>
      <td data-label="Rol">Lider</td>
    </tr>
    <tr>
      <td scope="row" data-label="IdU">0002</td>
      <td data-label="Nombre">Ignacio</td>
      <td data-label="Apellido">Perez</td>
      <td data-label="Rol">Seguridad</td>
    </tr>
    <tr>
      <td scope="row" data-label="IdU">0002</td>
      <td data-label="Nombre">Rodrigo</td>
      <td data-label="Apellido">Gil</td>
      <td data-label="Rol">Tallerista</td>
    </tr>
    <tr>
      <td scope="row" data-label="IdU">0002</td>
      <td data-label="Nombre">Gonzalo</td>
      <td data-label="Apellido">Garcia</td>
      <td data-label="Rol">Tallerista</td>
    </tr>
  </tbody>
</table>




  <div class="final">
    
  </div>




</section>

<?php } ?>


<!-- FIN DE VISUALIZAR EQUIPOS --> 

<!-- AGREGAR EQUIPO  --> 
<?php if($_SESSION['rol'] == 4 ) { ?>
<div class=" container equip " id="agregaT">
  <table class="agreg">
  <caption>AGREGAR EQUIPO</caption>
  <tbody>
    <form action="AgregarEquipo.php" method="post">
    <tr>
      <th scope="col">NOMBRE DEL EQUIPO </th>
      <td data-label="Equipo"><input type="text" required name="nome" value=""></td></tr>
        <tr>
      <td><a href="#"><input name="agregar"  type="submit" class="btn btn-primary" id="oka" value="Guardar Cambios"></input></a></td>
      <td><button class="btn btn-default" id="okc" >Cancelar</button></td>
    </tr>
  </form>
  </tbody>
</table>
</div>
<?php }?>

<?php if($_SESSION['rol'] == 4 ) { ?>
<div class=" container" id="eliminaT">
  <table class="elim">
  <caption>ELIMINAR EQUIPO</caption>
  <tbody>
    <form action="EliminarEquipo.php" method="post">
    <tr>
      <th scope="col" id="ch">NOMRE DEL EQUIPO</th>
      <td data-label="Usuario"><input type="text" required name="usu" value=""></td></tr>
    <tr>
      <td ><a href="#"><input  required name="eliminar"  type="submit" class="btn btn-danger" id="oke" value="Eliminar" onclick="confirmar();"></input></a></td>
      <td><button class="btn btn-default" id="okc" >Cancelar</button></td>
    </tr>
  </form>
  </tbody>
</table>
</div>

<!-- FIN ELIMINAR -->


<?php }?>


<!-- CONTENIDO DE ADMINISTRADOR --> 

<?php if($_SESSION['rol'] == 4){ ?>
<main class=" container">
  <?php }?>
  
  
  <?php if($_SESSION['rol'] == 4){ ?>
<div class="form-group busqueda">
  <div class="form-inline">

          <!-- PERMISO PARA AGREGAR O ELIMINAR USUARIOS --> 
         <?php if($_SESSION['rol'] == 4 ) { ?>  <button class=" boton-usuario btn btn-success  col-sm-12 col-md-5" id="agr">Agregar Usuario</button><?php }?>
         <?php if($_SESSION['rol'] == 4 ) { ?>   <button class=" boton-usuario btn btn-danger  col-sm-12 col-md-5" id="eliminando" >Eliminar Usuario</button><?php }?>
</div>
<div class ="clearfix"></div>
  <?php }?>
  
  
<!-- PERMISO DE FILTRADO DE INFORMACION  --> 

<?php if($_SESSION['rol'] == 4){ ?>
<h3><small>Búsqueda de equipos por filtro</small> </h3>
<div class="form-group busqueda">
  <div class="form-inline">
    <label class="sr-only">Ciudad</label>
    <select class="form-control" id="ciudad" name="Ciudad">
      <option>Busqueda por Ciudad</option>
      <option>Queretaro</option>
      <option>Chiapas</option>
    </select>

    <label class="sr-only">Colegio</label>
    <select class="form-control" id="Colegio" name="Colegio">
      <option>Busqueda por Colegio</option>
      <option>Mano Amiga </option>
      <option>Colegio Pedagogos</option>
    </select>

</div>
  </div>
 
<?php }?>

<!-- FIN DE PERMISO DE FILTRADO DE INFORMACION  --> 
<!-- VISUALIZAR EQUIPOS  --> 

<?php if($_SESSION['rol'] == 4){ ?>
<section class="main row ">
         <article class="col-xs-12 col-sm-12  col-md-12 demo">
          <div class="container  col-xs-6 col-sm-6 col-md-4 col-lg-3"><a href="Admin-EquipoN.php?idpag=13"><img class="img-responsive" src="Images/demo1.png"></a></div>
         <div class="container  col-xs-6 col-sm-6 col-md-4 col-lg-3"><img class="img-responsive" src="Images/demo2.png"></div>
          <div class="container  col-xs-6 col-sm-6 col-md-4 col-lg-3"><img class="img-responsive" src="Images/demo2.png"></div>
         <div class="container  col-xs-6 col-sm-6 col-md-4 col-lg-3"><img class="img-responsive" src="Images/demo2.png"></div>
         <div class="container  col-xs-6 col-sm-6 col-md-4 col-lg-3"><img class="img-responsive" src="Images/demo2.png"></div>
          <div class="container  col-xs-6 col-sm-6 col-md-4 col-lg-3"><img class="img-responsive" src="Images/demo2.png"></div>
         <div class="container  col-xs-6 col-sm-6 col-md-4 col-lg-3"><img class="img-responsive" src="Images/demo2.png"></div>
          <div class="container  col-xs-6 col-sm-6 col-md-4 col-lg-3"><img class="img-responsive" src="Images/demo2.png"></div>
                   <div class="container  col-xs-6 col-sm-6 col-md-4 col-lg-3"><img class="img-responsive" src="Images/demo2.png"></div>
          <div class="container  col-xs-6 col-sm-6 col-md-4 col-lg-3"><img class="img-responsive" src="Images/demo2.png"></div>
         <div class="container  col-xs-6 col-sm-6 col-md-4 col-lg-3"><img class="img-responsive" src="Images/demo2.png"></div>
          <div class="container  col-xs-6 col-sm-6 col-md-4 col-lg-3"><img class="img-responsive" src="Images/demo2.png"></div>
                   <div class="container  col-xs-6 col-sm-6 col-md-4 col-lg-3"><img class="img-responsive" src="Images/demo2.png"></div>
          <div class="container  col-xs-6 col-sm-6 col-md-4 col-lg-3"><img class="img-responsive" src="Images/demo2.png"></div>
         <div class="container  col-xs-6 col-sm-6 col-md-4 col-lg-3"><img class="img-responsive" src="Images/demo2.png"></div>
          <div class="container  col-xs-6 col-sm-6 col-md-4 col-lg-3"><img class="img-responsive" src="Images/demo2.png"></div>
  
         </article>
     </section>






<?php } ?>

<!-- FIN DE VISUALIZAR EQUIPO  --> 


<!-- AGREGAR EQUIPO   --> 

<?php if($_SESSION['rol']==4){?>

<div class="container col-md-12">
  <img src="Images/agregar.png" class="pull-right agregar img-responsive img-circle" id="agreq">
  
</div>

<?php }?>

<!-- FIN DE  AGREGAR EQUIPO   --> 

<?php if($_SESSION['rol']==4){?>

</main>


<?php }?>















    <!-- Scripts -->



      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
        <script>




  $(document).ready(function(){
/* AGREGAR */
    $("#agregaT").hide();
     $("#eliminaT").hide();
    $("#actualizaT").hide();
  $("#agreq").hide();

    $("#agr").click(

      function(){

      $("#eliminaT").hide();
      $("#actualizaT").hide();
      $("#agregaT").fadeIn(700);



          $("#oka").click(function(){
          if(validar_usuario()){
            alert("Usuario Agregado de manera exitosa");
      $("#agregaT").fadeOut(700);
        }


        $("#okc").click(function(){

           confirm("Seguro de Cancelar?");
            $("#agregaT").fadeOut(700);


        });
       

    });
    
    
    
    
    
    




    var modal = document.getElementById("agregaT");
   window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

    });



/*ACTUALIZAR*/

    $("#actualizando").click(function(){

      $("#eliminaT").hide();
      $("#agregaT").hide();
      $("#actualizaT").fadeIn(700);


    });


    $("#okat").click(function(){

      $("#actualizaT").hide();


    });



 
/* ELIMINAR */

   

    $("#eliminando").click(function(){
      $("#agregaT").hide();
      $("#actualizaT").hide();
      $("#eliminaT").fadeIn(700);

       var modal = document.getElementById("eliminaT");

     window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}



    $("#oke").click(function(){

      $("#eliminaT").hide();


    });

    });

$(document).scroll(function() {
  var y = $(this).scrollTop();
  if (y > 400) {
    $('#agreq').fadeIn(500);
    
    $("#agreq").click(function(){
      
       $("#agregaT").fadeIn(700);
      $("#actualizaT").hide();
      $("#eliminaT").hide();
      
      
          $("#okc").click(function(){

      $("#agregaT").fadeOut(500);


    });
      
      
      
      
      
      
      
    });
    
    
    
    
    
  } else {
    $('#agreq').fadeOut(500);
  }
});

  





  });

</script>
  </body>
</html>