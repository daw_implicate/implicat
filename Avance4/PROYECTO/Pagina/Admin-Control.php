<!doctype html>
<html lang="en">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="maincolors.css">
   <link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans" rel="stylesheet"> 

    <title>Admin-Control</title>
  </head>
  <body>
    <!-- SESSION -->


<?php require("sesion_activa.php") ;
    if($_SESSION['rol'] !==4){  

          header("Location:../Login/UA.php");

       } 
       
include("Partials/Header.php") ;?>

<?php


include("Partials/Navegacion.php");


?>


<?php if($_SESSION['rol'] == 4) { ?>

    <aside class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
     
       

    </aside>

<main class="container">
       <article class=" tablas container">
      <div class="row header container ">
        <div class="logo navbar-left">
          <h2> <b>BUSQUEDA DE ARCHIVOS </b></h2>
        </div>

      </div>
      
<?php }?>

<!-- BUSQUEDA  -->


<?php if($_SESSION['rol'] == 4) { ?>

<div class="form-group busqueda ">
  <div class="form-inline navbar-left">
    <label class="sr-only">Seleccionar Fecha</label>
    <select class="form-control" id="ciudad" name="Ciudad">
      <option> Seleccionar Fecha</option>
      <option>02/02/18</option>
      <option>08/02/18</option>
    </select>
    <label class="sr-only">Busqueda por Ciudad</label>
    <select class="form-control" id="ciudad" name="Ciudad">
      <option> Seleccionar Ciudad</option>
      <option>Queretaro</option>
      
    </select>
    <label class="sr-only">Busqueda por Institucion</label>
    <select class="form-control" id="ciudad" name="Ciudad">
      <option> Seleccionar Institucion</option>
      <option>Mano Amiga</option>
      <option>Colegio Pedagogos</option>
    </select>
    <label class="sr-only">Seleccionar Equipo</label>
    <select class="form-control" id="ciudad" name="Ciudad">
      <option> Seleccionar Equipo</option>
      <option>02/02/18</option>
      <option>08/02/18</option>
    </select>
    <label class="sr-only">Seleccionar Rol</label>
    <select class="form-control" id="ciudad" name="Ciudad">
      <option> Seleccionar Rol</option>
      <option>Queretaro</option>
      
    </select>

    

      
  </div>
</div>

 
 <?php }?>   





<?php if($_SESSION['rol'] == 4) { ?>

<table>
  <caption>Trabajos</caption>
  <thead>
    <tr>
      <th scope="col">Fecha</th>
      <th scope="col">Ciudad</th>
      <th scope="col">Institucion</th>
      <th scope="col">Autor</th>
      <th scope="col">Equipo</th>
      <th scope="col">Rol</th>
      <th scope="col">Trabajo</th>
      
    </tr>
  </thead>
  <tbody>
    <tr>
      <td data-label="Fecha">02/02/18</td>
      <td data-label="Ciudad">Queretaro</td>
      <td data-label="Institucion">Mano Amiga</td>
      <td data-label="Autor">Manuel Mejia</td>
      <td data-label="Equipo">5</td>
      <td data-label="Rol">Liderista</td>
      <td data-label="Trabajo"><a href="#">Plantilla Liderista</a></td>
    </tr>
    <tr>
      <td data-label="Fecha">08/02/18</td>
      <td data-label="Ciudad">Queretaro</td>
      <td data-label="Institucion">Mano Amiga</td>
      <td data-label="Autor">Roberto Andrade </td>
      <td data-label="Equipo">1</td>
      <td data-label="Rol">Seguridad</td>
      <td data-label="Trabajo"><a href="#">Plantilla Seguridad</a></td>
    </tr>
    <tr>
      <td data-label="Fecha">08/02/18</td>
      <td data-label="Ciudad">Queretaro</td>
      <td data-label="Institucion">Mano Amiga</td>
      <td data-label="Autor">Pedro Perez</td>
      <td data-label="Equipo">9</td>
      <td data-label="Rol">Tallerista</td>
      <td data-label="Trabajo"><a href="#">Plantilla Tallerista</a></td>
    </tr>
    <tr>
      <td data-label="Fecha">16/02/18</td>
      <td data-label="Ciudad">Queretaro</td>
      <td data-label="Institucion">Mano Amiga</td>
      <td data-label="Autor">Daniel Mendez</td>
      <td data-label="Equipo">2</td>
      <td data-label="Rol">Liderista</td>
      <td data-label="Trabajo"><a href="#">Lista Asistencia</a></td>
    </tr>
  </tbody>
</table>


</div>
</article>


</main>
<!-- TABLA DE TRABAJOS  -->




<?php }?>
















    <!-- Scripts -->



      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
   <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>-->
  </body>
</html>