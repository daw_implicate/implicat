<!doctype html>
<html lang="en">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans" rel="stylesheet"> 
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="maincolors.css">

    <title>Admin-Equipos</title>
  </head>
  <body>



    <!-- SESSION -->
<?php require("sesion_activa.php") ;
include("Partials/Header.php") ;?>







<!-- HEADER -->



<!--
      Documento 


-->



<!-- USUARIOS AUTORIZADOS -->
<?php


include("Partials/Navegacion.php");


?>

<!-- FIN DE BARRA DE NAVEGACION -->

<!-- CONTENIDO DEL USUARIO  -->
<!-- VISUALIZAR EQUIPO  -->
<?php if($_SESSION['rol'] == 4) { include("Partials/GeneralesEquipo.php"); }?>

<br><br><br>

<div class="row">
  <div class="container botonesE">
    <a href="#"><button class="btn btn-success" id="agr">AGREGAR</button></a>
    <a href="#"><button class="btn btn-danger" id="eliminando">ELIMINAR</button></a>

  </div>
</div>



<section class="main row">



<!-- AGREGAR ELIMINAR -->

<?php if($_SESSION['rol'] == 4 ) { ?>

<div class=" container" id="agregaT">
  <table class="agreg">
  <caption>AGREGAR INTEGRANTE</caption>

  <tbody>
    <form action="AgregarUE.php" method="post">
    <tr>
     <th scope="col">Usuario</th>
      <td data-label="Usuario"><input type="text" name="usu" placeholder="Seleccionar Usuario"></td></tr>
    <tr>
      <th scope="col">Seleccionar Rol</th>
       <td data-label="Rol"><select class="form-control" id="rol" name="Rol">
      
      <option>Lider</option>
      <option>Seguridad</option>
      <option>Tallerista</option>
    </select></td>
      
    </tr>
    <tr>
      <td><a href="#"><button class="btn btn-primary" id="oka">Guardar Cambios</button></a></td>
      <td><a href="#"><button class="btn btn-default" id="okc">Cancelar</button></a></td>
    </tr>
  </form>
  </tbody>
</table>
</div>

<?php }?>

<?php if($_SESSION['rol'] == 4 ) { ?>
<div class=" container" id="eliminaT">
  <table class="elim">
  <caption>ELIMINAR INTEGRANTE</caption>
  <tbody>
    <form action="EliminarUsuario.php" method="post">
    <tr>
      <th scope="col" id="ch">Usuario</th>
      <td data-label="Usuario"><input type="text" required name="usu" value=""></td></tr>
    <tr>
      <td ><a href="#"><input  required name="eliminar"  type="submit" class="btn btn-danger" id="oke" value="Eliminar" onclick="confirmar();"></input></a></td>
      <td><a href="#"><button class="btn btn-default" id="okc">Cancelar</button></a></td>
    </tr>
  </form>
  </tbody>
</table>
</div>

<!-- FIN ELIMINAR -->


<?php }?>



<!--FIN AGREGAR ELIMINAR-->
  <article class="container">





<!-- INTEGRANTES -->
<table>
  <caption>Integrantes</caption>
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Nombre</th>
      <th scope="col">Apellido</th>
      <th scope="col">Rol</th>
  </thead>
  <tbody>
    <tr>
      <td data-label="IdU">0001</td>
      <td data-label="Nombre">Andres</td>
      <td data-label="Apellido">Gonzalez</td>
      <td data-label="Rol">Lider</td>

    </tr>
    <tr>
      <td scope="row" data-label="IdU">0002</td>
      <td data-label="Nombre">Ignacio</td>
      <td data-label="Apellido">Perez</td>
      <td data-label="Rol">Seguridad</td>

    </tr>
    <tr>
      <td scope="row" data-label="IdU">0002</td>
      <td data-label="Nombre">Rodrigo</td>
      <td data-label="Apellido">Gil</td>
      <td data-label="Rol">Tallerista</td>

    </tr>
    <tr>
      <td scope="row" data-label="IdU">0002</td>
      <td data-label="Nombre">Gonzalo</td>
      <td data-label="Apellido">Garcia</td>
      <td data-label="Rol">Tallerista</td>

    </tr>
  </tbody>
</table>
<table>
        <th scope="col"><a href="Admin-Equipo-EditarRol.php">Editar Rol</a></th>
    </tr>
</table>
<!-- BUSQUEDA  -->

<div class="form-group busqueda">
  <div class="form-inline">
    <label class="sr-only">Busqueda por fecha</label>
    <select class="form-control" id="fechaA" name="fecha">
      <option> Busqueda por Fecha</option>
      <option>02/02/18</option>
      <option>08/02/18</option>
    </select>
</div>
    
<!-- TABLA DE TRABAJOS  -->

<table>
  <caption>Trabajos</caption>
  <thead>
    <tr>
      <th scope="col">Tipo</th>
      <th scope="col"></th>
      <th scope="col"></th>
      
    </tr>
  </thead>
  <tbody>
    <tr>
      <td data-label="tipo">Plantilla Liderista</td>
      <td data-label=""><a href="#">Ver</a></td>
      <td data-label=""><a href="#">Descargar</a></td>
    </tr>
    <tr>
      <td  scope ="row" data-label="tipo">Plantilla Seguridad</td>
      <td data-label=""><a href="#">Ver</a></td>
      <td data-label=""><a href="#">Descargar</a></td>
    </tr>
    <tr>
      <td scope="row" data-label="tipo">Plantilla Tallerista</td>
      <td data-label=""><a href="#">Ver</a></td>
      <td data-label=""><a href="#">Descargar</a></td>
    </tr>
    <tr>
      <td data-label="tipo">Lista Asistencia</td>
      <td data-label=""><a href="#">Ver</a></td>
      <td data-label=""><a href="#">Descargar</a></td>
    </tr>
  </tbody>
</table>

</div></article>


    <!--LOGO DEL COLEGIO -->




</section>

<div class="row col-md-2">
  <a href="Admin.php"><img src="Images/atras.png" class=" atras img-responsive"></a>
</div>


<footer >
 <div class=" container-fluid borde">
   
 </div>
</footer>














    <!-- Scripts -->



      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>

        <script >
  
  $(document).ready(function(){
/* AGREGAR */
    $("#agregaT").hide();
     $("#eliminaT").hide();
    $("#actualizaT").hide();


    $("#agr").click(function(){

      $("#eliminaT").hide();
      $("#actualizaT").hide();
      $("#agregaT").fadeIn(700);

       var modal = document.getElementById("agregaT");

     window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

    });


    $("#oka").click(function(){

      $("#agregaT").fadeOut(700);


    });
/*ACTUALIZAR*/

    $("#actualizando").click(function(){

      $("#eliminaT").hide();
      $("#agregaT").hide();
      $("#actualizaT").show();


    });


    $("#okat").click(function(){

      $("#actualizaT").hide();


    });





 
/* ELIMINAR */

   

    $("#eliminando").click(function(){
      $("#agregaT").hide();
      $("#actualizaT").hide();
      $("#eliminaT").show();
      
   var modal = document.getElementById("eliminaT");

     window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

    });


    $("#oke").click(function(){

      $("#eliminaT").hide();


    });

  });

</script>





   <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>-->
  </body>
</html>