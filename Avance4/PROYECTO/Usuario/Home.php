<!doctype html>
<html lang="en">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans" rel="stylesheet"> 
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="mainUser.css">

    <title>Usuario-Inicio</title>
  </head>
  <body>

    <!-- SESSION -->
<?php require("sesion_activa.php") ?>







<!-- HEADER -->
   <header >
    <div class="container-fluid mitad1" >
      <img src="Images/img-nav2.png" class="img-responsive barra">
      <?php
     echo "<h4 class='saludo'> Usuario : ". $_SESSION["usuario"]."</h4>";
      ?> 




    </div>

</header>


<!--
      Documento 


-->
<div class="header_bg img-responsive">
  <div class="container">
    
    <div class="row header ">
      
    <div class="logo navbar-left">
     <h2>Bienvenido</h2>
    </div>
    <div class="clearfix"></div>
    
  </div>
 
</div>



<!-- USUARIOS AUTORIZADOS -->
<?php

if($_SESSION['rol'] == 3 || $_SESSION['rol'] == 2){


  include("Partials/NavegacionUsuario.php");

}



?>

<!-- FIN DE BARRA DE NAVEGACION -->

<div class="container">
    <div class="row header ">
     <div class="navbar-left">
    <h2 class="subtitulo"> <b>Equipo N </b></h2>
    </div>  
    <div class="navbar-right foto">
     
        <img src="Images/demo2.png" class="img-responsive">
      </div>











    </div>
  </div>
</div>
 <div class="clearfix"></div>

<br><br><br>

<div class="container-fluid fondo">
<div class="container carrete"> 
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="Images/carrete1.png" alt="Los Angeles" width="100%">
      </div>

      <div class="item">
        <img src="Images/carrete2.png" alt="Chicago" width="100%">
      </div>
    
      <div class="item">
        <img src="Images/carrete3.jpg" alt="New york" width="100%">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Anterior</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Siguiente</span>
    </a>
  </div>
</div>

</div>



<div class="container">





<section class="main row">



<!-- AGREGAR ELIMINAR -->


  <article class="container">

<br><br><br>
<p></p>
<p></p>


<!-- INTEGRANTES -->
<table>
  <caption>Integrantes de Equipo</caption>
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Nombre</th>
      <th scope="col">Apellido</th>
      <th scope="col">Rol</th>
  </thead>
  <tbody>
    <tr>
      <td data-label="IdU">0001</td>
      <td data-label="Nombre">Andres</td>
      <td data-label="Apellido">Gonzalez</td>
      <td data-label="Rol">Lider</td>
    </tr>
    <tr>
      <td scope="row" data-label="IdU">0002</td>
      <td data-label="Nombre">Ignacio</td>
      <td data-label="Apellido">Perez</td>
      <td data-label="Rol">Seguridad</td>
    </tr>
    <tr>
      <td scope="row" data-label="IdU">0002</td>
      <td data-label="Nombre">Rodrigo</td>
      <td data-label="Apellido">Gil</td>
      <td data-label="Rol">Tallerista</td>
    </tr>
    <tr>
      <td scope="row" data-label="IdU">0002</td>
      <td data-label="Nombre">Gonzalo</td>
      <td data-label="Apellido">Garcia</td>
      <td data-label="Rol">Tallerista</td>
    </tr>
  </tbody>
</table>




  <div class="final">
    
  </div>




</section>























    <!-- Scripts -->



      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
   <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>-->
  </body>
</html>