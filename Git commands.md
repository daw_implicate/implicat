Git clone ...
(branch) git add *
(branch) git commit -m "add first files"
(branch) git push origin (branch) 
(branch) git pull / git pull origin master
(master) git checkout -b develop
(develop) git add *
(develop) git commit -m "Add JS"
(develop) git push origin develop
(develop) git checkout -b charlations/layout
(charlations/layout) git add *
(charlations/layout) git commit -m "Infinitivo"
(charlations/layout) git push origin charlations/layout
(develop) git checkout master
(master) git merge develop
(master) git push origin master
Para ver en que rama estoy: git branch

git push origin --delete <branch_name> (borrar rama en repositorio)
git branch -d branch_name (borrar rama localmente)

git ls-files --deleted -z | xargs -0 git rm (Sirve para que git deje de rastrear documentos eliminados)

Si manda errores al hacer el pull lo mas probable es que tengas archivos que no dejan hacer el pull correctamente, lo mas
f�cil es copiar a un folder temporal los cambios que hayas hecho, borrar toda la carpeta del repositorio y hacer un clone
para tener el repositorio al dia, posteriormente se realizan los pasos para crear su rama(mencionados abajo) e insertan 
nuevamente sus archivos(ya en su rama) a la carpeta para posteriormente hacer el commit y merge y que se tomen en cuenta.
Recuerden que en master y develop no se hacen commits solo en sus ramas de trabajo, hay 2 opciones, la recomendada es que 
cada que trabajen en una parte del proyecto creen una rama a partir de develop que tenga el nombre de lo que haran y seria
algo asi:

git checkout develop
git pull (si es la primera vez se hace esto y luego el pull: git branch --set-upstream-to origin develop)
git checkout -b "Lo que hare"

La otra manera seria borrar la rama en la que trabajas localmente cada que se trabajara para que continue actualizandose 
con develop y seria algo asi:

git checkout develop
git branch -d "El nombre de su rama"	(Estas 2 acciones causaran que tu rama se actualize con develop y evitar futuros 
git branch -b "El nombre de su rama"	 conflictos en el merge)

Trabajan en la rama que acaban de crear y despues se realiza lo siguiente las veces que sea necesario:

git add *
git commit -m "Lo que hicieron"
git push origin master (si van a hacer varios push vale la pena hacer: git branch --set-upstream-to origin "El nombre de su rama")

Una vez este terminado el trabajo en tu rama y hayan hecho el commit correctamente se hace lo siguiente:

git checkout develop
git merge "El nombre de su rama"
git push

Cuando todo el equipo concuerde o sea una entrega de equipo se hace merge con el master:

git checkout master
git merge develop
git push