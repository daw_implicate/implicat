<!doctype html>
<html lang="en">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="maincolors.css">
    <link rel="stylesheet" type="text/css" href="eventos.css">
<link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans" rel="stylesheet"> 
    <title>Admin-Control-TALLERES</title>
  </head>
  <body>
<?php 
require("sesion_activa.php") ;
    if($_SESSION['rol'] !==4){  

          header("Location:../Login/UA.php");

       } 

include("Partials/Header.php") ;
include("Partials/Navegacion.php");

  
?>



<!---***************************************************-->
<div class="clearfix"> <br><br><br></div>



<!---***************************************************-->
<?php if($_SESSION['rol'] == 4) { ?>
<div class="row">
  <div class="container botonesE">
    <a href="#"><button class="btn btn-success" id="agr">AGREGAR</button></a>
    <button class="btn btn-danger" id="eliminando">ELIMINAR</button></a></td>

  </div>

<?php }?>

<?php if($_SESSION['rol'] == 4) { ?>
<div class=" container" id="agregaT">
  <table>
  <caption>AGREGAR</caption>

  <tbody>
    <form action="AgregarTaller.php" method="post">
    <tr>
     <th scope="col">NOMBRE</th>
      <td data-label="Nombre"><input type="text" name="nombre" value="EventoN"></td>
    
      
    </tr>
    <tr>
      <td ><a href="#"><input type="submit" class="btn btn-primary" id="oka" value="GUARDAR "></input></a></td>
      <td ><a href="#"><button class="btn btn-default" id="okc">CANCELAR</button></a></td>

    </tr>
    </form>
  </tbody>
</table>
</div>
<?php }?>

<?php if($_SESSION['rol'] == 4) { ?>
<div class=" container" id="eliminaT">
  <table>
  <caption>ELIMINAR</caption>

  <tbody>
    <form action="EliminarTaller.php" method="post">
    <tr>
      <th scope="col">ID TALLER </th>
      <td data-label="Nombre"><input type="text" name="idta" value="0001"></td>
      
    </tr>
    <tr>
      <td ><a href="#"><input type="submit" class="btn btn-danger" id="oke" value="Eliminar"></input></a></td>
      <td ><a href="#"><button class="btn btn-default" id="okc">CANCELAR</button></a></td>
    </tr>
  </form>
  </tbody>
</table>
</div>
<?php }?>

<?php if($_SESSION['rol'] == 4) { ?>
<div class=" container" id="actualizaT">
  <table>
  <caption>ACTUALIZAR</caption>
  <thead>
    <tr>
    
      <th scope="col">NOMBRE</th>
      <td data-label="Nombre"><input type="text" name="nombre" value="Evento18"></td>
      
      </tr>
  </thead>
  <tbody>
    <tr>
      <td ><a href="#"><button class="btn btn-info" id="okat">Actualizar</button></a></td>
    </tr>
  </tbody>
</table>
</div>
<?php }?>


<?php if($_SESSION['rol'] == 4) { ?>
<div class="container">
<table>
  <caption>TALLERES</caption>
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">NOMBRE</th>
       <th scope="col"></th>
          
      
      
  </thead>
  <tbody>
    <?php foreach($matrizTalleres as $taller) : ?>
    <tr>
      <td data-label="IdT"><?php echo $taller['idTaller']?></td>
      <td data-label="Nombre"><?php echo $taller['NombreTaller']?></td>
      <td data-label="Nombre"><a href="#"><button class="btn btn-default" id="actualizando"><small>Editar</small></button></a></td>
      
     
      
    </tr>

  <?php endforeach?>

  </tbody>
</table>
</div>
<?php }?>
<aside class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
     
       

    </aside>

<footer >
 <div class=" container-fluid borde">
   
 </div>
</footer>














    <!-- Scripts -->



      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
        <script >
  
  $(document).ready(function(){
/* AGREGAR */
    $("#agregaT").hide();
     $("#eliminaT").hide();
    $("#actualizaT").hide();


    $("#agr").click(function(){

      $("#eliminaT").hide();
      $("#actualizaT").hide();
      $("#agregaT").show();
    var modal = document.getElementById("agregaT");

     window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


      $("#oka").click(function(){

      $("#agregaT").hide();


    });



    });



/*ACTUALIZAR*/

    $("#actualizando").click(function(){

      $("#eliminaT").hide();
      $("#agregaT").hide();
      $("#actualizaT").show();


    });


    $("#okat").click(function(){

      $("#actualizaT").hide();


    });



 
/* ELIMINAR */

   

    $("#eliminando").click(function(){
      $("#agregaT").hide();
      $("#actualizaT").hide();
      $("#eliminaT").show();
    var modal = document.getElementById("eliminaT");

     window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

    $("#oke").click(function(){

      $("#eliminaT").hide();


    });


    });



  });

</script>
  </body>
</html>