<!doctype html>
<html lang="en">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="maincolors.css">
<link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans" rel="stylesheet"> 
    <title>Admin-Usuarios</title>
  </head>
  <body>
<!-- SESION ACTIVA PHP -->

<?php require("sesion_activa.php"); 
      include("UsuarioControlador.php");?>




<!-- HEADER -->
   <header >
    <div class="container-fluid mitad1" >
      <img src="Images/img-nav2.png" class="img-responsive barra">
      <?php
     echo "<h4 class='saludo'> Usuario : ". $_SESSION["usuario"]."</h4>";
      ?> 




    </div>

</header>


<!--
      Documento 


-->
<div class="header_bg img-responsive">
  <div class="container">
    
    <div class="row header ">
      
    <div class="logo navbar-left">
      <h1>
        GESTIÓN DE USUARIOS
      </h1>
    </div>
    <div class="clearfix"></div>
    
  </div>
 
</div>
<nav class="navbar navbar-default  menu container">
  <div class="container navi">
    <div class="navbar-header">
      
    </div>
    <ul class="nav navbar-nav ">
      <li ><a href="Admin.php">EQUIPOS</a></li>
      <li class="activa" ><a href="Admin-Usuarios.php">USUARIOS</a></li>
      <li><a href="Admin-Control.php">ARCHIVOS</a></li>
      <li><a href="Admin-Eventos.php">CONTROL</a></li>
    </ul>
    <a href="cierre_sesion.php"><img src="Images/logout.png" class="img-responsive salir"></a>
  </div>
</nav>

</div>
<div class="clearfix"></div>



<!---***************************************************-->

<section class="main row">

    <article class="container">
<!-- Filtro de Usuarios-->
<div class="form-group busqueda">
  <div class="form-inline">

          
           <button class=" boton-usuario btn btn-primary  col-sm-12 col-md-5" id="agr">Agregar Usuario</button>
            <button class=" boton-usuario btn btn-danger  col-sm-12 col-md-5" id="eliminando">Eliminar Usuario</button>
</div>



<!-- AGREGAR EDITAR -->
<!--
<div class=" container" id="agregaT">
  <table class="agreg">
  <caption>AGREGAR INTEGRANTE</caption>
  <thead>
    <tr>
     
      <th scope="col">Usuario</th>
      <th scope="col">Contrasena</th>
      <th scope="col">Nombre</th>
      <th scope="col">Apellido Paterno</th>
      <th scope="col">Apellido Materno</th>
      <th scope="col">Telefono</th>
      </tr>
  </thead>
  <tbody>
    <form action="AgregarUsuario.php" method="post">
    <tr>
     
      <td data-label="Usuario"><input type="text" required name="usu" value=""></td>
      <td data-label="Contrasena"><input  required type="password" name="pass" value=""></td>
      <td data-label="Nombre"><input required type="text" name="nom" value=""></td>
      
      <td data-label="Apellido"><input required type="text" name="apep" value=""></td>
      <td data-label="Apellido M"><input required type="text" name="apm" value=""></td>
      <td data-label="Telefono"><input required type="text" name="tel" value=""></td>
    
      
    </tr>
    <tr>
      <td   colspan="5"><a href="#"><input name="agregar"  type="submit" class="btn btn-primary" id="oka" value="Agregar"></input></a></td>
    </tr>
  </form>
  </tbody>
</table>
</div>

-->

<!-- OPCION 2 AGREGAR -->

<div class=" container" id="agregaT">
  <table class="agreg">
  <caption>AGREGAR INTEGRANTE</caption>
  <tbody>
    <form action="AgregarUsuario.php" method="post">
    <tr>
      <th scope="col">Usuario</th>
      <td data-label="Usuario"><input type="text" required name="usu" value=""></td></tr>
      <tr>
      <th scope="col">Contrasena</th>
      <td data-label="Contrasena"><input  required type="password" name="pass" value=""></td></tr>
      <tr>
        <th scope="col">Nombre</th>
      <td data-label="Nombre"><input required type="text" name="nom" value=""></td></tr>
      <tr>
        <th scope="col">Apellido Paterno</th>
      <td data-label="Apellido"><input required type="text" name="apep" value=""></td></tr>
      <tr>
        <th scope="col">Apellido Materno</th>
      <td data-label="Apellido M"><input required type="text" name="apm" value=""></td></tr>
      <tr>
       <th scope="col">Telefono</th>
      <td data-label="Telefono"><input required type="text" name="tel" value=""></td></tr>
    
      
    </tr>
    <tr>
      <td   colspan="2"><a href="#"><input name="agregar"  type="submit" class="btn btn-primary" id="oka" value="Guardar Cambios"></input></a></td>
    </tr>
  </form>
  </tbody>
</table>
</div>






<!--  FIN OPCION 2 AGREGAR -->

<!--  ELIMINAR -->

<div class=" container" id="eliminaT">
  <table class="elim">
  <caption>ELIMINAR INTEGRANTE</caption>
  <tbody>
    <form action="EliminarUsuario.php" method="post">
    <tr>
      <th scope="col" id="ch">Usuario</th>
      <td data-label="Usuario"><input type="text" required name="usu" value=""></td></tr>
    <tr>
      <td   colspan="2"><a href="#"><input  required name="eliminar"  type="submit" class="btn btn-danger" id="oke" value="Eliminar" onclick="confirmar();"></input></a></td>
    </tr>
  </form>
  </tbody>
</table>
</div>

<!-- FIN ELIMINAR -->
<div class=" container" id="actualizaT">
  <table class="agreg">
  <caption>EDITAR INTEGRANTE</caption>
  <thead>
    <tr>
     
      <th scope="col">NOMBRE</th>
      <th scope="col">APELLIDO PATERNO </th>
      <th scope="col"> APELLIDO MATERNO</th>
      <th scope="col">DIRECCION</th>
      <th scope="col">TELEFONO</th>
      </tr>
  </thead>
  <tbody>
    <tr>
     
      <td data-label="Nombre"><input type="text" name="nombre" value="Adrian"></td>
      <td data-label="ApellidoP"><input type="text" name="nombre" value="Valdezz"></td>
      <td data-label="ApellidoM"><input type="text" name="nombre" value="Juarez"></td>
      <td data-label="Direccion"><input type="text" name="nombre" value="Robles 28"></td>
      <td data-label="telefono"><input type="text" name="nombre" value="44332837"></td>

    
      
    </tr>
    <tr>
      <td   colspan="5"><a href="#"><button class="btn btn-default" id="okat">Actualizar</button></a></td>
    </tr>
  </tbody>
</table>
</div>


<!--FIN AGREGAR ELIMINAR-->
<!-- ORDENAR -->


<div class="container"></div>
    <label class="sr-only">Ordenar Por</label>
    <select  class="form-control ord" id="ordenarU" name="ordenarU">
      <option>Ordenar Por</option>
      <option>ID</option>
      <option>Nombre</option>
      <option>Equipo</option>
      <option>Rol</option>
    </select>



  </div>







<!-- INTEGRANTES -->


<table>
  <caption>Usuarios</caption>
  <thead>
    <tr>
       <th scope="col">USUARIO</th>
      <th scope="col">NOMBRE</th>
      <th scope="col">APELLIDO PATERNO</th>
      <th scope="col">APELLIDO MATERNO</th>
      <th scope="col">TELEFONO</th>
      <th scope="col">STATUS</th>
      <th></th>
    </tr>
  </thead>
  <tbody>

<?php foreach($matrizUsuarios as $usuario) : ?>
    <tr>
      <td data-label="Usuario"><?php echo $usuario['username'] ?></td>
      <td data-label="Nombre"><?php echo $usuario['nombre'] ?></td>
      <td data-label="Apellido Paterno"><?php echo $usuario['apellidoPaterno'] ?></td>
      <td data-label="Apellido Materno"><?php echo $usuario['apellidoMaterno'] ?></td>
      <td data-label="Telefono"><?php echo $usuario['telefono'] ?></td>
      <td data-label="Status"><?php echo $usuario['status']?></td>
      <td data-label="Nombre"><a href="#"><input type="submit" class="btn btn-default" id="actualizando" value="Editar"></input></a></td>
     <!-- <td data-label="Nombre"><a href="#"><input name="actionEntry"  type="submit" class="btn btn-danger" id="eliminando" value="Eliminar"></input></a></td>-->
    </tr>
  <?php endforeach ?>
   <!-- <tr>
      <td scope="row" data-label="IdU">0002</td>
      <td data-label="Nombre">Ignacio</td>
      <td data-label="Apellido">Perez</td>
      <td data-label="Equipo">5</td>
      <td data-label="Rol">Seguridad</td>
      <td data-label="Nombre"><a href="#"><button class="btn btn-default" id="actualizando"><small>Editar</small></button></a></td>
      <td data-label="Nombre"><a href="#"><button class="btn btn-danger" id="eliminando"><small>Eliminar</small></button></a></td>
    </tr>
    <tr>
      <td scope="row" data-label="IdU">0003</td>
      <td data-label="Nombre">Rodrigo</td>
      <td data-label="Apellido">Gil</td>
      <td data-label="Equipo">3</td>
      <td data-label="Rol">Tallerista</td>
      <td data-label="Nombre"><a href="#"><button class="btn btn-default" id="actualizando"><small>Editar</small></button></a></td>
      <td data-label="Nombre"><a href="#"><button class="btn btn-danger" id="eliminando"><small>Eliminar</small></button></a></td>
    </tr>
    <tr>
      <td scope="row" data-label="IdU">0004</td>
      <td data-label="Nombre">Gonzalo</td>
      <td data-label="Apellido">Garcia</td>
      <td data-label="Equipo">8</td>
      <td data-label="Rol">Tallerista</td>
      <td data-label="Nombre"><a href="#"><button class="btn btn-default" id="actualizando"><small>Editar</small></button></a></td>
      <td data-label="Nombre"><a href="#"><button class="btn btn-danger" id="eliminando"><small>Eliminar</small></button></a></td>
    </tr>
    <tr>
      <td data-label="IdU">0005</td>
      <td data-label="Nombre">Luis</td>
      <td data-label="Apellido">Grijalva</td>
      <td data-label="Equipo">1</td>
      <td data-label="Rol">Seguridad</td>
      <td data-label="Nombre"><a href="#"><button class="btn btn-default" id="actualizando"><small>Editar</small></button></a></td>
      <td data-label="Nombre"><a href="#"><button class="btn btn-danger" id="eliminando"><small>Eliminar</small></button></a></td>
    </tr>
    <tr>
      <td scope="row" data-label="IdU">0006</td>
      <td data-label="Nombre">Rosalio</td>
      <td data-label="Apellido">Herrera</td>
      <td data-label="Equipo">4</td>
      <td data-label="Rol">Lider</td>
      <td data-label="Nombre"><a href="#"><button class="btn btn-default" id="actualizando"><small>Editar</small></button></a></td>
      <td data-label="Nombre"><a href="#"><button class="btn btn-danger" id="eliminando"><small>Eliminar</small></button></a></td>
    </tr>
    <tr>
      <td scope="row" data-label="IdU">0007</td>
      <td data-label="Nombre">Mateo</td>
      <td data-label="Apellido">Benavides</td>
      <td data-label="Equipo">9</td>
      <td data-label="Rol">Seguridad</td>
      <td data-label="Nombre"><a href="#"><button class="btn btn-default" id="actualizando"><small>Editar</small></button></a></td>
      <td data-label="Nombre"><a href="#"><button class="btn btn-danger" id="eliminando"><small>Eliminar</small></button></a></td>
    </tr>
    <tr>
      <td scope="row" data-label="IdU">0008</td>
      <td data-label="Nombre">Ramiro</td>
      <td data-label="Apellido">Gonzales</td>
      <td data-label="Equipo">1</td>
      <td data-label="Rol">Tallerista</td>
      <td data-label="Nombre"><a href="#"><button class="btn btn-default" id="actualizando"><small>Editar</small></button></a></td>
      <td data-label="Nombre"><a href="#"><button class="btn btn-danger" id="eliminando"><small>Eliminar</small></button></a></td>
    </tr>-->
  </tbody>
</table>


</article>


    <!--LOGO DEL COLEGIO -->

    <aside class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
     
       

    </aside>


</section>




<footer >
 <div class=" container-fluid borde">
   
 </div>
</footer>














    <!-- Scripts -->



      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>

        <script >
  
  $(document).ready(function(){
/* AGREGAR */
    $("#agregaT").hide();
     $("#eliminaT").hide();
    $("#actualizaT").hide();


    $("#agr").click(function(){

      $("#eliminaT").hide();
      $("#actualizaT").hide();
      $("#agregaT").fadeIn(700);


    });


    $("#oka").click(function(){

      $("#agregaT").fadeOut(700);


    });
/*ACTUALIZAR*/

    $("#actualizando").click(function(){

      $("#eliminaT").hide();
      $("#agregaT").hide();
      $("#actualizaT").fadeIn(700);


    });


    $("#okat").click(function(){

      $("#actualizaT").hide();


    });



 
/* ELIMINAR */

   

    $("#eliminando").click(function(){
      $("#agregaT").hide();
      $("#actualizaT").hide();
      $("#eliminaT").fadeIn(700);


    });


    $("#oke").click(function(){

      $("#eliminaT").hide();


    });

  });


/* CONFIRMAR */

function confirmar(){
  confirm("Estas seguro de eliminar a este usuario ");
}


</script>
  </body>
</html>