<!doctype html>
<html lang="en">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans" rel="stylesheet"> 
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="maincolors.css">

    <title>Admin-Equipos</title>
  </head>
  <body>
<!-- SESION ACTIVA PHP -->

<?php require("sesion_activa.php"); ?>




<!-- HEADER -->
<!-- HEADER -->
   <header >
    <div class="container-fluid mitad1" >
      <img src="Images/img-nav2.png" class="img-responsive barra">
      <?php
     echo "<h4 class='saludo'> Usuario : ". $_SESSION["usuario"]."</h4>";
      ?> 




    </div>

</header>


<!--
      Documento 


-->
<div class="header_bg img-responsive">
  <div class="container">
    
    <div class="row header ">
      
    <div class="logo navbar-left">
      <h1>
      EDICIÓN DE EQUIPO
      </h1>
    </div>
    <div class="clearfix"></div>
    
  </div>
 
</div>
<nav class="navbar navbar-default  menu container">
  <div class="container navi">
    <div class="navbar-header">
      
    </div>
    <ul class="nav navbar-nav ">
      <li class="activa"><a href="Admin.php">EQUIPOS</a></li>
      <li ><a href="Admin-Usuarios.php">USUARIOS</a></li>
      <li><a href="Admin-Control.php">ARCHIVOS</a></li>
      <li><a href="Admin-Eventos.php">CONTROL</a></li>
    </ul>
    <a href="cierre_sesion.php"><img src="Images/logout.png" class="img-responsive salir"></a>
  </div>
</nav>

</div>
<div class="clearfix"></div>

<div class="container">
    <div class="row header">
     <div class="navbar-left">
    <h2 class="subtitulo"> <b>Equipo N </b></h2>
    </div>  
    <div class="navbar-right foto">
     
        <img src="Images/demo2.png" class="img-responsive">
      </div>











    </div>
</div>
 <div class="clearfix"></div>
<!--
<div class="row">
  <div class="container botonesE">
    <a href="#"><button class="btn btn-primary" id="agr">AGREGAR</button></a>
    <a href="#"><button class="btn btn-danger" id="eli">ELIMINAR</button></a>
  </div>
</div>
-->
<section class="main row">

<!-- AGREGAR ELIMINAR -->

<!--<div class=" container" id="agregaT">
  <table>
  <caption>AGREGAR INTEGRANTE</caption>
  <thead>
    <tr>
     
      <th scope="col">ID</th>

      </tr>
  </thead>
  <tbody>
    <tr>
     
      <td data-label="Nombre"><input type="text" name="nombre" value="0005"></td>
    
      
    </tr>
    <tr>
      <td ><a href="#"><button class="btn btn-primary" id="oka">Agregar</button></a></td>
    </tr>
  </tbody>
</table>
</div>
<div class=" container" id="eliminaT">
  <table>
  <caption>ELIMINAR INTEGRANTE</caption>
  <thead>
    <tr>
     
      <th scope="col">ID</th>

      </tr>
  </thead>
  <tbody>
    <tr>
     
      <td data-label="Nombre"><input type="text" name="nombre" value="0005"></td>
    
      
    </tr>
    <tr>
      <td ><a href="#"><button class="btn btn-danger" id="oke">ELIMINAR</button></a></td>
    </tr>
  </tbody>
</table>
</div>

-->

<!--FIN AGREGAR ELIMINAR-->

  <article class="container">





<!-- INTEGRANTES -->
<table>
  <caption>Integrantes</caption>
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Nombre</th>
      <th scope="col">Apellido</th>
      <th scope="col">Rol</th>
  </thead>
  <tbody>
    <tr>
      <td data-label="IdU">0001</td>
      <td data-label="Nombre">Andres</td>
      <td data-label="Apellido">Gonzalez</td>
            <td data-label="Rol"><select class="form-control" id="rol" name="Rol">
      <option> Seleccionar Rol</option>
      <option>Lider</option>
      <option>Seguridad</option>
      <option>Tallerista</option>
    </select></td>
    </tr>
    <tr>
      <td scope="row" data-label="IdU">0002</td>
      <td data-label="Nombre">Ignacio</td>
      <td data-label="Apellido">Perez</td>
            <td data-label="Rol"><select class="form-control" id="rol" name="Rol">
      <option> Seleccionar Rol</option>
      <option>Lider</option>
      <option>Seguridad</option>
      <option>Tallerista</option>
    </select></td>
    </tr>
    <tr>
      <td scope="row" data-label="IdU">0002</td>
      <td data-label="Nombre">Rodrigo</td>
      <td data-label="Apellido">Gil</td>
            <td data-label="Rol"><select class="form-control" id="rol" name="Rol">
      <option> Seleccionar Rol</option>
      <option>Lider</option>
      <option>Seguridad</option>
      <option>Tallerista</option>
    </select></td>
    </tr>
    <tr>
      <td scope="row" data-label="IdU">0002</td>
      <td data-label="Nombre">Gonzalo</td>
      <td data-label="Apellido">Garcia</td>
            <td data-label="Rol"><select class="form-control" id="rol" name="Rol">
      <option> Seleccionar Rol</option>
      <option>Lider</option>
      <option>Seguridad</option>
      <option>Tallerista</option>
    </select></td>
    </tr>
  </tbody>
</table>
<table>
        <th scope="col"><a href="Admin-EquipoN.php">GUARDAR CAMBIOS</a></th>
    </tr>
</table>
<!-- BUSQUEDA  -->

<div class="form-group busqueda">
  <div class="form-inline">
    <label class="sr-only">Busqueda por fecha</label>
    <select class="form-control" id="fechaA" name="fecha">
      <option> Busqueda por Fecha</option>
      <option>02/02/18</option>
      <option>08/02/18</option>
    </select>
</div>
    
<!-- TABLA DE TRABAJOS  -->

<table>
  <caption>Trabajos</caption>
  <thead>
    <tr>
      <th scope="col">Tipo</th>
      <th scope="col"></th>
      <th scope="col"></th>
      
    </tr>
  </thead>
  <tbody>
    <tr>
      <td data-label="tipo">Plantilla Liderista</td>
      <td data-label=""><a href="#">Ver</a></td>
      <td data-label=""><a href="#">Descargar</a></td>
    </tr>
    <tr>
      <td  scope ="row" data-label="tipo">Plantilla Seguridad</td>
      <td data-label=""><a href="#">Ver</a></td>
      <td data-label=""><a href="#">Descargar</a></td>
    </tr>
    <tr>
      <td scope="row" data-label="tipo">Plantilla Tallerista</td>
      <td data-label=""><a href="#">Ver</a></td>
      <td data-label=""><a href="#">Descargar</a></td>
    </tr>
    <tr>
      <td data-label="tipo">Lista Asistencia</td>
      <td data-label=""><a href="#">Ver</a></td>
      <td data-label=""><a href="#">Descargar</a></td>
    </tr>
  </tbody>
</table>

</div></article>


    <!--LOGO DEL COLEGIO -->




</section>

<div class="row col-md-2">
  <a href="Admin.php"><img src="Images/atras.png" class=" atras img-responsive"></a>
</div>


<footer >
 <div class=" container-fluid borde">
   
 </div>
</footer>














    <!-- Scripts -->



      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>





                <script >
  
  $(document).ready(function(){
/* AGREGAR */
    $("#agregaT").hide();
     $("#eliminaT").hide();
    

    $("#agr").click(function(){

      $("#eliminaT").hide();
      
      $("#agregaT").show();


    });


    $("#oka").click(function(){

      $("#agregaT").hide();


    });




 
/* ELIMINAR */

   

    $("#eli").click(function(){
      $("#agregaT").hide();
     
      $("#eliminaT").show();


    });


    $("#oke").click(function(){

      $("#eliminaT").hide();


    });

  });

</script>

   <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>-->
  </body>
</html>