<!doctype html>
<html lang="en">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans" rel="stylesheet"> 
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="mainUser.css">

    <title>Usuario-Pregunta</title>
  </head>
  <body>


    <!-- SESION ACTIVA PHP -->



<!-- HEADER -->
   <header >
    <div class="container-fluid mitad1" >
      <img src="Images/img-nav2.png" class="img-responsive">
      
    </div>

</header>


<!--
      Documento 


-->
<div class="header_bg img-responsive">
  <div class="container">
    
    <div class="row header ">
      
    <div class="logo navbar-left">
      <h1>
      CUESTIONARIO 
      </h1>
    </div>
    <div class="clearfix"></div>
    
  </div>
 
</div>
<nav class="navbar navbar-default  menu container">
  <div class="container navi">
    <div class="navbar-header">
      
    </div>
    <ul class="nav navbar-nav ">
      <li ><a href="Home.php">EQUIPO </a></li>
      <li><a href="Usuario-Equipo.php">ASISTENCIA</a></li>
      <li class="activa" ><a href="Usuario-Preguntas.php">PREGUNTAS</a></li>
      <li><a href="Usuario-Fotos.php">FOTOS</a></li>
    </ul>
    <a href="cierre_sesion.php"><img src="Images/logout.png" class="img-responsive salir"></a>
  </div>
</nav>

</div>
<div class="clearfix"></div>

<div class="container">
    <div class="row header">
     <div class="navbar-left">
    <h2 class="subtitulo"> <b>Equipo N </b></h2>
    </div>  
    <div class="navbar-right foto">
     
        <img src="Images/demo2.png" class="img-responsive">
      </div>











    </div>
</div>
 <div class="clearfix"></div>

<div class="container">
<table>
  <caption>MOMENTOS DE LA SESION</caption>
  <thead>
    <tr>
      <th scope="col" colspan="7"></th>
      
    
      
      
  </thead>
  <tbody>
        <tr>
      <td data-label="IdU" colspan="2"><select class="primero"><option>Taller</option><span></select><select><option>Tallerista</option></select></span></td>
      <td data-label="Nombre" colspan="5"><textarea></textarea></td>
     
      
    </tr>
        <tr>
      <td data-label="IdU" colspan="2"><select class="primero"><option>Taller</option><span></select><select><option>Tallerista</option></select></span></td>
      <td data-label="Nombre" colspan="5"><textarea></textarea></td>
     
      
    </tr>
        <tr>
      <td data-label="IdU" colspan="2"><select class="primero"><option>Taller</option><span></select><select><option>Tallerista</option></select></span></td>
      <td data-label="Nombre" colspan="5"><textarea></textarea></td>
     
      
    </tr>
        <tr>
      <td data-label="IdU" colspan="2"><select class="primero"><option>Taller</option><span></select><select><option>Tallerista</option></select></span></td>
      <td data-label="Nombre" colspan="5"><textarea></textarea></td>
     
      
    </tr>

    <tr>
      <td colspan="7"><h4>SECCION 1</h4></td>
    </tr>
    <tr>
      <td colspan="2"> <h6>Preguntas</h6></td>
      <td >Muy Mal</td><td >Mal</td>
      <td >Regular</td>
      <td >Bien</td>
      <td >Muy Bien</td>
    </tr>
    <tr>
      <td colspan="2"><h6>En general, como te sentiste en la sesion</h6></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>



    </tr>
        <tr>
      <td colspan="2"><h6>¿Cómo consideras que fue tu rendimiento durante la sesión?</h6></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>



    </tr>
        <tr>
      <td colspan="2"><h6>¿Cómo fue la colaboración de los integrantes de tu equipo durante la sesión?</h6></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>



    </tr>
        <tr>
      <td colspan="2"><h6>¿Cumpliste con las acciones que te correspondían?</h6></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>



    </tr>
        <tr>
      <td colspan="2"><h6>¿Realizaste todas las actividades planeadas de la sesión?</h6></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>



    </tr>
   </tr>
        <tr>
      <td colspan="2"><h6>¿Utilizaste el valor, competencia o habilidad para la vida correspondiente en la sesión?</h6></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>



    </tr>
       </tr>
        <tr>
      <td colspan="2"><h6>¿Cómo percibiste la actitud de los niños durante la sesión?</h6></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>
      <td ><input type="radio" name=""></td>



    </tr>


    <tr>
      <td colspan="7"><h4>SECCION 2</h4></td>
    </tr>
    <tr>
      <td colspan="2"> <h6>¿Cómo te sentiste durante la sesión?</h6></td>
      <td colspan="5"><textarea></textarea></td>
    </tr>
        <tr>
      <td colspan="2"> <h6>¿Qué aprendizajes obtuviste en esta sesión?</h6></td>
      <td colspan="5"><textarea></textarea></td>
    </tr>
        <tr>
      <td colspan="2"> <h6>¿Cómo podrías mejorar tu práctica para la próxima sesión?</h6></td>
      <td colspan="5"><textarea></textarea></td>
    </tr>
        <tr>
      <td colspan="2"> <h6>¿Cómo percibiste el ambiente de la sesión?</h6></td>
      <td colspan="5"><textarea></textarea></td>
    </tr>
        <tr>
      <td colspan="2"> <h6>¿Cómo fue la organización y logística del equipo?</h6></td>
      <td colspan="5"><textarea></textarea></td>
    </tr>


<tr>
  <td colspan="5"></td>
  <td colspan="2">  <button type="submit" class="btn btn-success" onclick="Confirmacion()">Enviar</button></td>
</tr>


  </tbody>
</table>
</div>
<aside class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
     
       

    </aside>







<br>


</form>

</div>

<aside class="col-md-3">
  
</aside>








<footer >
 <div class=" container-fluid borde">
   
 </div>
</footer>














    <!-- Scripts -->



      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>


<script>
  function Confirmacion() {
    confirm("Estas listo para enviar la informacion?");
    
  }
</script>




   <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>-->
  </body>
</html>