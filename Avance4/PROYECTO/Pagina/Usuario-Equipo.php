<!doctype html>
<html lang="en">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans" rel="stylesheet"> 
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="mainUser.css">

    <title>Lista de Asistencia</title>
  </head>
  <body>


    <!-- SESION ACTIVA PHP -->

    <?php require("sesion_activa.php") ?>
    <?php if($_SESSION['rol'] !==3){  

          header("Location:../Login/UA.php");

       } ?> 




<!-- HEADER -->
<?php include("Partials/Header.php") ?>

<!--
      Documento 


-->





<!-- NAVEGACION -->



<?php


include("Partials/Navegacion.php");


?>
<?php if($_SESSION['rol'] == 3) { include("Partials/GeneralesEquipo.php"); }?>


<!--  FIN DE NAVEGACION -->






<section class="main row">



<!-- AGREGAR ELIMINAR -->


  <article class="container">


<!-- VER INTEGRANTES DE EQUIPO  -->

<?php if($_SESSION['rol'] == 3) { ?>


<!-- INTEGRANTES -->
<table>
  <caption>Integrantes de Equipo</caption>
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Nombre</th>
      <th scope="col">Apellido</th>
      <th scope="col">Rol</th>
      <?php if($_SESSION['rol'] == 3) { ?><th scope="col">Asistio</th> <?php } ?>
  </thead>
  <tbody>
    <tr>
      <td data-label="IdU">0001</td>
      <td data-label="Nombre">Andres</td>
      <td data-label="Apellido">Gonzalez</td>
      <td data-label="Rol">Lider</td>
      <?php if($_SESSION['rol'] == 3) { ?><td data-label="Asistio"><input type="checkbox" name="asistencia"></td> <?php }?>
    </tr>
    <tr>
      <td scope="row" data-label="IdU">0002</td>
      <td data-label="Nombre">Ignacio</td>
      <td data-label="Apellido">Perez</td>
      <td data-label="Rol">Seguridad</td>
      <td data-label="Asistio"><input type="checkbox" name="asistencia"></td>
    </tr>
    <tr>
      <td scope="row" data-label="IdU">0002</td>
      <td data-label="Nombre">Rodrigo</td>
      <td data-label="Apellido">Gil</td>
      <td data-label="Rol">Tallerista</td>
      <td data-label="Asistio"><input type="checkbox" name="asistencia"></td>
    </tr>
    <tr>
      <td scope="row" data-label="IdU">0002</td>
      <td data-label="Nombre">Gonzalo</td>
      <td data-label="Apellido">Garcia</td>
      <td data-label="Rol">Tallerista</td>
      <td data-label="Asistio"><input type="checkbox" name="asistencia"></td>
    </tr>

    <tr>
      <td></td><td></td><td></td>
<!-- GUARDAR CAMBIOS DE LA LISTA -->
      <?php if($_SESSION['rol'] == 3) { ?><td colspan="2"><button class="btn btn-primary" onclick="exito();">Guardar Cambios</button></td> <?php } ?>
    </tr>
  </tbody>
</table>

<?php } ?>



    <!--LOGO DEL COLEGIO -->




</section>




<footer >
 <div class=" container-fluid borde">
   
 </div>
</footer>














    <!-- Scripts -->



      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>


<script >
  
  function exito(){
    alert("Guardado con exito");

  }

</script>




   <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>-->
  </body>
</html>