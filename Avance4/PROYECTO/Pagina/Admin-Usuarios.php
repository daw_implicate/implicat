<!doctype html>
<html lang="en">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="maincolors.css">
<link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans" rel="stylesheet"> 
    <title>Admin-Usuarios</title>
  </head>
  <body>
<!-- SESION ACTIVA PHP -->



    <!-- SESSION -->
<?php 
require("sesion_activa.php") ;
    if($_SESSION['rol'] !==4){  

          header("Location:../Login/UA.php");

       } 
include("Partials/Header.php") ;
include("Partials/Navegacion.php");

  
?>






<!---***************************************************-->
<?php if($_SESSION['rol'] == 4 ) { ?>
<section class="main row">

    <article class="container">
<!-- Filtro de Usuarios-->
<div class="form-group busqueda">
  <div class="form-inline">

          <!-- PERMISO PARA AGREGAR O ELIMINAR USUARIOS --> 
         <?php if($_SESSION['rol'] == 4 ) { ?>  <button class=" boton-usuario btn btn-success  col-sm-12 col-md-5" id="agr">Agregar Usuario</button><?php }?>
         <?php if($_SESSION['rol'] == 4 ) { ?>   <button class=" boton-usuario btn btn-danger  col-sm-12 col-md-5" id="eliminando">Eliminar Usuario</button><?php }?>
</div>

<?php }?>


<?php if($_SESSION['rol'] == 4 ) { ?>
<div class=" container" id="agregaT">
  <table class="agreg">
  <caption>AGREGAR INTEGRANTE</caption>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
  <tbody>
    <form action="AgregarUsuario.php" method="post" onsubmit="return validar_usuario();">
    <tr>
      <th scope="col">Usuario <p id="usua" class="text-danger"></p></th>
      <td data-label="Usuario"><input type="text" required name="usu" id="usu" value=""></td></tr>
      <tr>
      <th scope="col">Contrasena<p id="passa" class="text-danger"></p></th>
      <td data-label="Contrasena"><input  required type="password" id="pass" name="pass" value=""></td></tr>
      <tr>
      <th scope="col">Confirmar Contrasena<p id="confpassa" class="text-danger"></p></th>
      <td data-label="Confirmar"><input  required type="password" id="confpass" name="confpass" value=""></td></tr>

      <tr>
        <th scope="col">Nombre<p id="noma" class="text-danger"></p></th>
      <td data-label="Nombre"><input required type="text" name="nom" id="nom" value=""></td></tr>
      <tr>
        <th scope="col">Apellido Paterno<p id="apepa" class="text-danger"></p></th>
      <td data-label="Apellido"><input required type="text" name="apep" id="apep" value=""></td></tr>
      <tr>
        <th scope="col">Apellido Materno<p id="apma" class="text-danger"></p></th>
      <td data-label="Apellido M"><input required type="text" name="apm"  id="apm" value=""></td></tr>
      <tr>
       <th scope="col">Telefono<p id="tela" class="text-danger"></p></th>
      <td data-label="Telefono"><input required type="text" name="tel"  id="tel" value=""></td></tr>
    
      
    </tr>
    <tr>
      <td ><a href="#"><input noame="agregar"  type="submit" class="btn btn-primary" id="oka" value="Guardar Cambios"></input></a></td>
      <td><button class="btn btn-default" id="okc" >Cancelar</button></td>
    </tr>
  </form>
  </tbody>
</table>
</div>
<?php }?>





<!--  FIN OPCION 2 AGREGAR -->

<!--  ELIMINAR -->
<?php if($_SESSION['rol'] == 4 ) { ?>
<div class=" container" id="eliminaT">
  <table class="elim">
  <caption>ELIMINAR INTEGRANTE</caption>
  <tbody>
    <form action="EliminarUsuario.php" method="post">
    <tr>
      <th scope="col" id="ch">Usuario</th>
      <td data-label="Usuario"><input type="text" required name="usu" value=""></td></tr>
    <tr>
      <td ><a href="#"><input  required name="eliminar"  type="submit" class="btn btn-danger" id="oke" value="Eliminar" onclick="confirmar();"></input></a></td>
      <td><button class="btn btn-default" id="okc" >Cancelar</button></td>
    </tr>
  </form>
  </tbody>
</table>
</div>

<!-- FIN ELIMINAR -->


<?php }?>

<?php if($_SESSION['rol'] == 4 ) { ?>
<div class=" container" id="actualizaT">
  <table class="agreg">
  <caption>EDITAR INTEGRANTE</caption>
  <thead>
    <tr>
     
      <th scope="col">NOMBRE</th>
      <th scope="col">APELLIDO PATERNO </th>
      <th scope="col"> APELLIDO MATERNO</th>
      <th scope="col">DIRECCION</th>
      <th scope="col">TELEFONO</th>
      </tr>
  </thead>
  <tbody>
    <tr>
     
      <td data-label="Nombre"><input type="text" name="nombre" value="Adrian"></td>
      <td data-label="ApellidoP"><input type="text" name="nombre" value="Valdezz"></td>
      <td data-label="ApellidoM"><input type="text" name="nombre" value="Juarez"></td>
      <td data-label="Direccion"><input type="text" name="nombre" value="Robles 28"></td>
      <td data-label="telefono"><input type="text" name="nombre" value="44332837"></td>

    
      
    </tr>
    <tr>
      <td   colspan="5"><a href="#"><button class="btn btn-default" id="okat">Actualizar</button></a></td>
    </tr>
  </tbody>
</table>
</div>
<?php }?>

<!--FIN AGREGAR ELIMINAR-->
<!-- ORDENAR -->

<?php if($_SESSION['rol'] == 4 ) { ?>
<div class="container"></div>
    <label class="sr-only">Ordenar Por</label>
    <select  class="form-control ord" id="ordenarU" name="ordenarU">
      <option>Ordenar Por</option>
      <option>ID</option>
      <option>Nombre</option>
      <option>Equipo</option>
      <option>Rol</option>
    </select>



  </div>
<?php }?>







<!-- INTEGRANTES -->

<?php if($_SESSION['rol'] == 4 ) { ?>
<table>
  <caption>Usuarios</caption>
  <thead>
    <tr>
       <th scope="col">USUARIO</th>
      <th scope="col">NOMBRE</th>
      <th scope="col">APELLIDO PATERNO</th>
      <th scope="col">APELLIDO MATERNO</th>
      <th scope="col">TELEFONO</th>
      <th scope="col">STATUS</th>
      <th></th>
    </tr>
  </thead>
  <tbody>

<?php foreach($matrizUsuarios as $usuario) : if($usuario['status'] == 'ACTIVO'){?>
    <tr>
      <td data-label="Usuario"><?php echo $usuario['username'] ?></td>
      <td data-label="Nombre"><?php echo $usuario['nombre'] ?></td>
      <td data-label="Apellido Paterno"><?php echo $usuario['apellidoPaterno'] ?></td>
      <td data-label="Apellido Materno"><?php echo $usuario['apellidoMaterno'] ?></td>
      <td data-label="Telefono"><?php echo $usuario['telefono'] ?></td>
      <td data-label="Status"><?php  echo $usuario['status']?></td>
      <td data-label="Nombre"><a href="#"><input type="submit" class="btn btn-default" id="actualizando" value="Editar"></input></a></td>
     <!-- <td data-label="Nombre"><a href="#"><input name="actionEntry"  type="submit" class="btn btn-danger" id="eliminando" value="Eliminar"></input></a></td>-->
    </tr>
  <?php  } endforeach ?>
  
  <?php }?>

  </tbody>
</table>


</article>


    <!--LOGO DEL COLEGIO -->

    <aside class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
     
       

    </aside>


</section>




<footer >
 <div class=" container-fluid borde">
   
 </div>
</footer>














    <!-- Scripts -->



      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script type="text/javascript" src ="js/validar.js"></script>
        <script >
  
  $(document).ready(function(){
/* AGREGAR */
    $("#agregaT").hide();
     $("#eliminaT").hide();
    $("#actualizaT").hide();


    $("#agr").click(

      function(){

      $("#eliminaT").hide();
      $("#actualizaT").hide();
      $("#agregaT").fadeIn(700);



          $("#oka").click(function(){
          if(validar_usuario()){
            alert("Usuario Agregado de manera exitosa");
      $("#agregaT").fadeOut(700);
        }


        $("#okc").click(function(){

           confirm("Seguro de Cancelar?");
            $("#agregaT").fadeOut(700);


        });
       

    });




    var modal = document.getElementById("agregaT");
   window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

    });



/*ACTUALIZAR*/

    $("#actualizando").click(function(){

      $("#eliminaT").hide();
      $("#agregaT").hide();
      $("#actualizaT").fadeIn(700);


    });


    $("#okat").click(function(){

      $("#actualizaT").hide();


    });



 
/* ELIMINAR */

   

    $("#eliminando").click(function(){
      $("#agregaT").hide();
      $("#actualizaT").hide();
      $("#eliminaT").fadeIn(700);

       var modal = document.getElementById("eliminaT");

     window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}



    $("#oke").click(function(){

      $("#eliminaT").hide();


    });

    });




  });


/* CONFIRMAR */

function confirmar(){
  confirm("Estas seguro de eliminar a este usuario ");
}


</script>
  </body>
</html>